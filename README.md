# My Homebrew Tap

Add my tap:

```shell
brew tap --force-auto-update francisferrell/tap https://gitlab.com/francisferrell/homebrew-tap.git
```

Then install a package:

```shell
brew install <formula>
```

