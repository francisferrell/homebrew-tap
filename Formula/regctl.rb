
class Regctl < Formula
  desc "Docker and OCI Registry Client"
  homepage "https://github.com/regclient/regclient"
  version "0.3.10"

  if Hardware::CPU.intel?
    url "https://github.com/regclient/regclient/releases/download/v0.3.10/regctl-darwin-amd64"
    sha256 "f98f0d77dad64c39e5d24720ea061cf94239a4c62be1cd2bd0bfc6e1d3fefb34"
  end

  if Hardware::CPU.arm?
    url "https://github.com/regclient/regclient/releases/download/v0.3.10/regctl-darwin-arm64"
    sha256 "39be36d4cd57f2981eb726a9d5ecd50a7bb61f4a242b5a746d7cf9c25f5d718c"
  end

  def install
    if Hardware::CPU.intel?
      bin.install "regctl-darwin-amd64" => "regctl"
    end

    if Hardware::CPU.arm?
      bin.install "regctl-darwin-arm64" => "regctl"
    end
  end

  test do
    system "#{bin}/regctl version"
  end
end

